<?php
class magaluRest
{
  public function __construct()
  {
    $encode = base64_encode(MAGALU_USER.':'.MAGALU_PASS);
    $this->magaluRest = new RestClient([
        'base_url' => "https://in.integracommerce.com.br",
         // https://dev.twitter.com/docs/auth/application-only-auth
        'headers' => [
          'accept' => 'application/json',
          'content-type' => 'application/json',
          'Authorization' => 'Basic '.$encode
        ],
      //   'headers' => [
      //     'username' => 'erviegas.api',
      //     'password' => 'erviegas.api'
      // ],
    ]);

    $this->magaluRestPut = new RestClient([
        'base_url' => "https://in.integracommerce.com.br",
         // https://dev.twitter.com/docs/auth/application-only-auth
        'headers' => [
          'Authorization' => 'Basic '.$encode
        ],
      //   'headers' => [
      //     'username' => 'erviegas.api',
      //     'password' => 'erviegas.api'
      // ],
    ]);
  }
}
