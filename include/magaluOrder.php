<?php
class magaluOrder extends magaluRest
{
  public function __construct()
  {
    parent::__construct();

  }

  public function magaluGetOrders()
  {
    $param = [
      'page' => 1,'perPage' => 100
    ];

    return json_decode($this->magaluRest->get("/api/Order",$param)->response);
  }

  public function magaluGetOrdersId()
  {
    $orders = $this->magaluGetOrders();

    if(is_null($orders->Orders)) return false;

    foreach ($orders->Orders as $key => $value) {
      $orderIdsList[] = $value->IdOrder;
    }

    return $orderIdsList;
  }

  public function magaluGetOrder($orderId)
  {
    $order = json_decode($this->magaluRest->get("/api/Order/$orderId")->response);

    return $order;
  }

  /* $orderStatusEntities
  *  PROCESSING {"IdOrder": "string","OrderStatus": "PROCESSING"}
  *  INVOICED   {"IdOrder": "string","OrderStatus": "INVOICED","InvoicedNumber": "string","InvoicedLine": 0,"InvoicedIssueDate": "2016-06-23T10:59:50.381Z","InvoicedKey": "string","InvoicedDanfeXml": "string"}
  *  SHIPPED    {"IdOrder": "string","OrderStatus": "SHIPPED","ShippedTrackingUrl": "string","ShippedTrackingProtocol": "string","ShippedEstimatedDelivery": "2016-06-23T10:59:50.381Z","ShippedCarrierDate": "2016-06-23T10:59:50.381Z","ShippedCarrierName": "string"}
  *  DELIVERED  {"IdOrder": "string","OrderStatus": "DELIVERED","DeliveredDate": "2016-06-23T10:59:50.381Z"}
  */
  public function magaluUpdateOrder($orderId,$orderStatusEntities)
  {
    $order = $this->magaluRest->put("/api/Order",$orderStatusEntities);

    return $order;
  }

  }
