<?php
if (!isset($prefix_magalu)) $prefix_magalu = '';

if(file_exists('include/php-restclient/vendor/autoload.php')) require_once $prefix_magalu.'include/php-restclient/vendor/autoload.php';
require_once $prefix_magalu.'include/config.php';
require_once $prefix_magalu.'include/defines.php';
require_once $prefix_magalu.'include/magaluRest.php';
require_once $prefix_magalu.'include/magaluProduct.php';
require_once $prefix_magalu.'include/magaluOrder.php';
 ?>
