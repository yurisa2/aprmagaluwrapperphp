<?php
class magaluProduct extends magaluRest
{
  public function __construct()
  {
    parent::__construct();

  }

  public function magaluGetProducts()
  {
    $param = [
      'page' => 1,'perPage' => 100
    ];

    return $this->magaluRest->get("/api/Sku",$param);
  }

  public function magaluGetProductsSku()
  {
    $products = (array)json_decode($this->magaluGetProducts()->response);

    foreach ($products['Skus'] as $key => $value) {
      $skuList[] = $value->IdSku;
    }

    return $skuList;
  }

  public function magaluCreateProduct($productData)
  {
    $body = [
      "IdProduct" => $productData->idProduct,
      "Name" => $productData->nameProduct,
      "Code" => $productData->codeProduct,
      "Brand" => $productData->brandProduct,
      "NbmOrigin" => 0,
      "WarrantyTime" => 90,
      "Active" => (bool)true,
      "Categories" => [ $productData->category  ]
    ];

    return $this->magaluRest->post("/api/Product", $body);
  }

  public function magaluCreateProductSku($productData)
  {
    $body = [
      "IdSku" => $productData->productSku,
      "IdSkuErp" => $productData->productSku,
      "IdProduct" => $productData->productId,
      "Name" => $productData->productName,
      "Description" => $productData->productDescription,
      "Height" => $productData->productHeight,
      "Width" => $productData->productWidth,
      "Length" => $productData->productLength,
      "Weight" => $productData->productWeight,
      "CodeEan" => '',
      // "CodeNcm" => '',
      // "CodeIsbn" => '',
      // "CodeNbm" => '$productData->',
      // "Variation" => '',
      "Status" => 'true',
      "Price" => [
        "ListPrice" => $productData->productPrice,
        "SalePrice" => $productData->productPrice
      ],
      "StockQuantity" => $productData->productQuantity,
      "MainImageUrl" => $productData->productMainImage,
      "UrlImages" => $productData->productImages,
      // "Attributes" => [
      //   [
      //     "Name" => "",
      //     "Value" => ""
      //   ]
      // ]
    ];

    return $this->magaluRest->post("/api/Sku", $body);
  }

  public function magaluUpdateProductSku($productData)
  {
    $body = array(
      "IdSku" => $productData['IdSku'],
      "IdProduct" => $productData['IdProduct'], // Id do produto
      "IdSkuErp" => $productData['IdSkuErp'], // Id do produto
      "Description" => $productData['Description'],
      "Name" => $productData['Name'], // Nome do Sku
      "Status" => 'true', // Status do Sku
      "Height" => $productData['Height'],
      "Width" => $productData['Width'],
      "Length" => $productData['Length'],
      "Weight" => $productData['Weight'],
      "StockQuantity" => $productData['StockQuantity'],
      "Price" => [
        "ListPrice" => $productData['ListPrice'],
        "SalePrice" => $productData['SalePrice']
        ],
      );

      return $this->magaluRestPut->put("/api/Sku", $body);
    }
  }
